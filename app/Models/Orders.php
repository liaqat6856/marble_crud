<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;
    public function user () {
        return $this->belongsTo(User::class);
    }
    function getData()
    {
        return $this->hasMany('App\Models\Inventory');
    }

    public function inventories(){
        return $this->hasMany(Inventory::class, 'order_id', 'id',);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
    public function suplier()
    {
        return $this->belongsTo(Supplier::class, 'suplier_id','id');
    }

}
