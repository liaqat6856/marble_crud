<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    public function user () {
        return $this->belongsTo(User::class);
    }
    public function inventory()
    {
        return $this->hasMany(Inventory::class, 'order_id','id');
    }
    public function orders()
    {
        return $this->hasMany(Orders::class, 'customer_id','id');
    }
    public function product()
    {
        return $this->hasMany(Product::class, 'product_name', 'id');
    }

}
