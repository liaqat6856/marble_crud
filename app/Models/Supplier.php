<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;
    public function user () {
        return $this->hasMany(User::class);
    }
    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'order_id','id');
    }
    public function order()
    {
        return $this->hasMany(Orders::class, 'suplier_id','id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_name', 'id');
    }
}
