<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'type', 'comments', 'image','created_at', 'updated_at'];
    public function user () {
        return $this->belongsTo(User::class);
    }
    public function product()
    {
        return $this->hasMany(Inventory::class, 'product_name', 'id');
    }

}
