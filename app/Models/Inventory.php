<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;
    public function user () {
        return $this->belongsTo(User::class);
    }

    // invers of has many
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_name', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function supplier()
    {
        return $this->hasMany(Supplier::class, 'name', 'id');
    }
}
