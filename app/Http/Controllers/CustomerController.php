<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\facades\DB;
use App\Models\Customer;
use App\Models\Inventory;
use App\Models\Orders;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *@param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        if($request->has('search'))
        {
          $name = $request->get('search');
          $customers=Customer::where('name', 'LIKE', '%'.$name.'%')->paginate(3);
          return view('customers.customer')->with('customers',$customers);
        }
        else
        {
            $customers = Customer::withCount('orders')->get();

            return view('customers.customer',compact('customers'));
        }
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.addcustomer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
           "name"=>"required",
           "phone"=>"required",
       ]);
       $data = new Customer;

        $data->name    =$request->input('name');
        $data->address =$request->input('address');
        $data->phone   =$request->input('phone');

        $data->save();
        return redirect('customers')->with('success', 'Customer Added');
    }

    public function storeAjax(Request $request)
    {
       $request->validate([
           "name"=>"required",
           "phone"=>"required",
       ]);
       $data = new Customer;

        $data->name    =$request->input('name');
        $data->address =$request->input('address');
        $data->phone   =$request->input('phone');

        $data->save();
        return  response()->json($data, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $shows = Orders::where('customer_id',$id)->get();
        return view('customers.show',compact('shows'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inventoryshow(Request $request, $id)
    {
        $inventories = Inventory::where('order_id', $id)->get();

        return view('customers.inventory',compact('inventories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers = Customer::find($id);
        return view('customers.edit')->with( 'customers', $customers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $request->validate([
                "name"=>"required",
                "phone"=>"required",
            ]);
         $data = Customer::find($id);

         $data->name    =$request->input('name');
         $data->address =$request->input('address');
         $data->phone   =$request->input('phone');

         $data->save();
         return redirect('/customers')->with('success', 'Customer Added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customers =  Customer::find($id);

        $customers->delete();

        return redirect('customers')->with('success', 'customers Deleted');
    }
    public function result(Request $request){
        if($request->isMethod('post'))
          {
            $name = $request->get('search');
            $customers=Customer::where('name', 'LIKE', '%'.$name.'%')->paginate(3);

          }
          return view('customers.customer')->with('customers',$customers);
    }
}
