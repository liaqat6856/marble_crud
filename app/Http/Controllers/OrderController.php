<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Mail\OrderMail;
use App\Models\Orders;
use App\Models\Inventory;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\Customer;
use Carbon\Carbon;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *@param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {   

        if($request->has('order_type')) {
            if($request->order_type=='outbound') {
                $customers = Customer::all();
                $suppliers = Supplier::all();
                $orders = Orders::where('order_type','outbound')->get();
                return view('orders', compact('orders'));
            }
             else if ($request->order_type=='intboud') {
            
                $customers = Customer::all();
                $suppliers = Supplier::all();
                $orders = Orders::where('order_type','intboud')->get();
                return view('orders', compact('orders'));
            } 
            }
            else if($request->has('search')){
                $search = $request->input('search');
                $search = Carbon::parse($request->input('search'))->format('Y-m-d');
                $orders = Orders::query()
                ->where('date', 'LIKE', "%{$search}%")
                ->get();
              
                return view('orders', compact('orders'));
                }
                else if($request->has('supplier_name'))
                {
                    $suppliers = Supplier::all(); 
                    return view('suppliers.index', compact('suppliers'));
                }
                else if($request->has('customer_name'))
                {
                    $customers = Customer::all(); 
                    return view('customers.customer', compact('customers'));
                }
                else 
                {

                    $orders =Orders::all();
                    $customers = Customer::all();
                    $suppliers = Supplier::all();
                    return view('orders', compact('orders'));
                }
        
            

    }
    public function wasteDetail()
    {
        $products = Product::all();
        return view('orders.waste',compact('products'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::all();
        $products = Product::all();
        return view('orders.receive',compact('suppliers','products'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer()
    {
        $products = Product::all();
        $customers = Customer::all();
        return view('orders.customer',compact('customers','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "date"=>"required",
            "supliern"=>"required",
            "customer_id"=>"null",
            "bill"=>"required",
            "vehicle"=>"required",
        ]);

        $data = new Orders;
        $data->date        = get_formatted_date($request->input('date'),'y-m-d');;
        $data->suplier_id   = $request->input('supliern');
        $data->bill_id      = $request->input('bill');
        $data->vehicle_no   = $request->input('vehicle');
        $data->comments     = $request->input('order_comments');
        $data->order_type   = "inboud";
        $data->save();


        for($i = 0; $i < sizeof($request->qty); $i++)
        {
            $items[] = [
                'order_id' => $data->id,
                'product_name' => $request->product_id[$i],
                'type' => $request->type[$i],
                'length' => $request->length[$i],
                'order_type' => 'inbound',
                'width' => $request->width[$i],
                'qty' => $request->qty[$i],
                'price' => $request->price[$i],
                'totelRs' => $request->totalrs[$i],
                'comments' => $request->comments[$i]
            ];
        }
        Inventory::insert($items);
        Mail::to('liaqatali6417@gmail.com')->send(new OrderMail( $data ));
        return redirect('orders')->with('success','Order Add');

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function customerstore(Request $request)
    {
        $request->validate([
            "date"=>"required",
            "customer_id"=>"required",
            "supliern"=>"null",
            "bill"=>"required",
            "vehicle"=>"null",
            "order_type"=>"null",
        ]);
        $data = new Orders;
        // $data->date        = \Carbon\Carbon::parse($request->input('date'))->format('y-m-d');
        $data->date        = get_formatted_date($request->input('date'),'y-m-d');
        $data->customer_id   = $request->input('customer_id');
        $data->bill_id      = $request->input('bill');
        $data->comments     = $request->input('order_comments');
        $data->order_type = "outbound";

        $data->save();
        for($i = 0; $i < sizeof($request->qty); $i++)
        {
            $items[] = [
                'order_id'     => $data->id,
                'product_name' => $request->product_id[$i],
                'type'         => $request->type[$i],
                'length'       => $request->length[$i]* -1,
                'width'        => $request->width[$i]* -1,
                'qty'          => $request->qty[$i]* -1,
                'order_type'   =>  "outbound",
                'price'        => $request->price[$i],
                'totelRs'      => $request->totalrs[$i],
                'comments'     => $request->comments[$i]

            ];
        }
        $product =Product::all();
        $id = $request->product_id;
        $userProduct = new ProductController();

        $dataCheck =  $userProduct->getStockOfSingleProduct($id);

       if($request->qty < $dataCheck && $request->length < $dataCheck && $request->width < $dataCheck){

         Inventory::insert($items);
         Mail::to('liaqatali6417@gmail.com')->send(new OrderMail($data));
         return redirect('orders')->with('success','Order Add');
       }else{

         return redirect('orders/customer')->with('error', 'Stock Not Available');
       }
       $this->OrderMailSend($orders);
    }

    // create order for waste
    public function wasteOrder(Request $request)

    {
        $request->validate([
            "date"=>"required",
            "customer_id"=>"null",
            "supliern"=>"null",
            "bill_id"=>"null",
            "vehicle"=>"null",
            "order_type"=>"null",
        ]);

        $data = new Orders;
        $data->date        = Carbon::parse($request->input('date'))->format('y-m-d');
        // $data->bill_id    = $request->input('bill_id');
        $data->comments    = $request->input('order_comments');
        $data->order_type  = "outbound";

        $data->save();
        for($i = 0; $i < sizeof($request->qty); $i++)
        {
            $items[] = [
                'order_id'     => $data->id,
                'product_name' => $request->product_id[$i],
                'type'         => $request->type[$i],
                'length'       => $request->length[$i]* -1,
                'width'        => $request->width[$i]* -1,
                'qty'          => $request->qty[$i]* -1,
                'order_type'   =>  "outbound",
                'price'        => $request->price[$i],
                'totelRs'      => $request->totalrs[$i],
                'comments'     => $request->comments[$i]

            ];
        }
        $product =Product::all();
        $id = $request->product_id;
        $userProduct = new ProductController();

        $data =  $userProduct->getStockOfSingleProduct($id);

       if($request->qty < $data && $request->length < $data && $request->width < $data){

          Inventory::insert($items);
          Mail::to('liaqatali6417@gmail.com')->send(new OrderMail());
          return redirect('orders')->with('success','Waste Order Add');
       }else{

         return redirect('orders/waste')->with('error', 'Stock Not Available');
       }
    }
    public function OrderMailSend($orders)
    {
      Mail::to($orders->email)->send(new OrderMail($orders));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orders = Orders::find($id);
        return view('orders.show')->with( 'orders', $orders);
    }
    public function show1()
    {
        // $orders = DB::table('orders')->where('order_type','inboud');
        $orders = Orders::where('customer_id')->get();


        return view('orders')->with( 'orders', $orders);
    }
      public function show2()
    {
        $orders = Orders::where('suplier_id')->get();

        return view('orders')->with( 'orders', $orders);
    }
/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showinventory(Request $request , $id)
    {

        $lists = Inventory::where('order_id', $id)->get();
        return view('inventory.list', compact('lists'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers = Supplier::all();
        $products = Product::all();
        $orders =Orders::find($id);
        $inventory =Inventory::find($id);
        return view('orders.edit',compact('suppliers','products','orders','inventory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = new Orders;

        $data->date=$request->input('date');
        $data->suplier_id=$request->input('supliern');
        $data->bill_id =$request->input('bill');
        $data->vehicle_no=$request->input('vehicle');
        $data->comments=$request->input('comments');

        $data->save();

        $inwentory = new Inventory;
        $inwentory->product_name = $request->input('product_id');
        $inwentory->type = $request->input('type');
        $inwentory->length = $request->input('length');
        $inwentory->width = $request->input('width');
        $inwentory->qty = $request->input('qty');
        $inwentory->price = $request->input('price');
        $inwentory->totelRs = $request->input('totalrs');
        $inwentory->comments = $request->input('comments');

        $inwentory->save();


        return redirect('orders')->with('success','Order Add');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders =  Orders::find($id);
        $orders->delete();
        $inventory = Inventory::where('order_id',$id);
        $inventory->delete();
        return redirect('orders')->with('success', 'Orders Deleted');
    }
    // public function SendMail(){
       
    //     $data = ['name' => "Liaqat Ali", 'data'=>'hello order'];
    //     $user['to'] = 'liaqatali6417@gamil.com';
        
    //     Mail::send('mail', $data, function($massages) use($user){
    //         $massages->to( $user['to']);
    //         $massages->subject('Hello Dev');
    //     });
    // }
    public function export() 
    {
        return Excel::download(new UsersExport, 'orders.xlsx');
    }
    public function exportCSV() 
    {
        return Excel::download(new UsersExport, 'orders.csv');
    }

}
