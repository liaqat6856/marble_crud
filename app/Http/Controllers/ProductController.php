<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Product;
use App\Models\Inventory;


class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->get();

        return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            "name"=>"required",
            "type"=>"required",
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $data =  $request->all();
        if($request->hasFile('image'))
        {
            $destination_path = 'public/products';
            $image            = $request->file('image');
            $image_name       = $image->getClientOriginalName();
            $path             = $request->file('image')->storeAs($destination_path, $image_name);

            $data['image']   = $image_name;
        }
        Product::create($data);
        session()->flash('massage','Successfully upload image');

        return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product =Product::find($id);

        $inventories = DB::table('inventories')->where('product_name',$product->id)
           ->get();

        $inventory = DB::table('inventories')->where('product_name',$product->id)->select(
         \DB::raw("SUM(width) as total_width"),
         \DB::raw("SUM(length) as total_length"),
         \DB::raw("SUM(qty) as total_qty")
        )
        ->get();

        return view('products.show', compact('inventory','inventories','product'));

        // dd($single_list->name );
        // return view('products.show')->with( 'product', $products);
    }
         /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function getStockOfSingleProduct($id)
        {
            $product =Product::find($id);
                foreach($product as $product){
                    $product->id;
            }

            $inventory = DB::table('inventories')->where('product_name',$product->id)->select(
                \DB::raw("SUM(width) as total_width"),
                \DB::raw("SUM(length) as total_length"),
                \DB::raw("SUM(qty) as total_qty")
               )
               ->get();



             $total = $inventory;
             return $total;
        }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $single_list =Product::find($id);

        return view('products.edit')->with( 'product', $single_list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data1 =  Product::find($id);
        
        $data1->name=$request->input('name');
        $data1->type=$request->input('type');
        $data1->comments=$request->input('comments');
        if($request->hasFile('image'))
        {
            $destination_path = 'public/products';
            $image            = $request->file('image');
            $image_name       = $image->getClientOriginalName();
            $path             = $request->file('image')->storeAs($destination_path, $image_name);

            $data1['image']   = $image_name;
        }
        $data1->save();
        session()->flash('massage','Successfully Edit image');

        return redirect('products');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product =  Product::find($id);

        $product->delete();

        return redirect('products')->with('success', 'Product Deleted');
    }
    public function result(Request $request){
        if($request->isMethod('post'))
          {
            $name = $request->get('search');
            $products=Product::where('name', 'LIKE', '%'.$name.'%')->paginate(2);

          }
          return view('products.index')->with('products',$products);
    }
}
