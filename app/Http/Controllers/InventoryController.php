<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inventory;
use App\Models\Product;
use DB;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
      $lists = Inventory::paginate(4);

        return view('inventory.list',compact('lists'));
    }
    public function result(Request $request){
        if($request->isMethod('post'))
          {
            $name = $request->get('search');
            $lists=Inventory::where('product_name', 'LIKE', '%'.$name.'%')->paginate(3);

          }
          return view('inventory.list', compact('lists'));
    }


    public function getData($id)
    {
        $product =Product::find($id)->first();
        $products = Inventory::select(
            "product_name",
            DB::raw("count(type) as type"),
            DB::raw("SUM(length) as total_length"),
            DB::raw("SUM(width) as total_width"),
            DB::raw("SUM(qty) as total_qty")
         )
         ->groupBy("product_name")
         ->get();

        return view('inventory.stock', compact('products'));

    }
    public function getDataStock()

    {
        // $types = DB::table('inventories')->where('type')->get();
        // dd($types);
        $products = Inventory::select(
            "product_name",
            DB::raw("SUM(length) as total_length"),
            DB::raw("SUM(width) as total_width"),
            DB::raw("SUM(qty) as total_qty")
         )
         ->groupBy("product_name")
         ->get('type');

        return view('inventory.stock', compact('products'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
