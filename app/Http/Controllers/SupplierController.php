<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\Orders;
use App\Models\Inventory;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        
        if($request->has('search'))
        {
          $name = $request->get('search');
          $suppliers=Supplier::where('name', 'LIKE', '%'.$name.'%')->paginate(2);
          return view('suppliers.index')->with('suppliers',$suppliers);
        }else{
            $suppliers = Supplier::all();
            return view('suppliers.index')->with('suppliers',$suppliers);
        }
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            "name"=>"required",
            "address"=>"required",
            "phone"=>"required"
        ]);
      
        $data = new Supplier;
       
        $data->name=$request->input('name');
        $data->address=$request->input('address');
        $data->phone=$request->input('phone');
       
        $data->save();

        return redirect('suppliers')->with('success', 'Supplier Added');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shows = Orders::where('suplier_id',$id)->get();
        return view('suppliers.show',compact('shows'));
    }
    /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function inventoryshow($id)
        {
            $inventoryds = Inventory::where('order_id',$id)->get();
           
            return view('suppliers.inventory',compact('inventoryds'));
        }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers =Supplier::find($id);
         
        return view('suppliers.edit')->with( 'suppliers', $suppliers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "name"=>"required",
            "address"=>"required",
            "phone"=>"required"
        ]);
      
        $data =Supplier::find($id);
       
        $data->name=$request->input('name');
        $data->address=$request->input('address');
        $data->phone=$request->input('phone');
       
        $data->save();

        return redirect('suppliers')->with('success', 'Supplier Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier =  Supplier::find($id);

        $supplier->delete();

        return redirect('suppliers')->with('success', 'Suppliers Deleted');
    }
    // public function search(Request $request)
    // {
    //   if($request->isMethod('post'))
    //   {
    //     $name = $request->get('search');
    //     $suppliers=Supplier::where('name', 'LIKE', '%'.$name.'%')->paginate(3);
       
    //   }
    //   return view('suppliers',compact('suppliers'));
    // }
    public function result(Request $request){
        if($request->isMethod('post'))
          {
            $name = $request->get('search');
            $suppliers=Supplier::where('name', 'LIKE', '%'.$name.'%')->paginate(2);
           
          }
          return view('suppliers.index')->with('suppliers',$suppliers);
    }
}
