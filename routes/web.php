<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\DisplayController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\InventoryController;
use App\Mail\OrderMail;
use Illuminate\Support\Facades\Mail;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$user = App\Models\User::find(1);
Auth::login($user);

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
/*
|--------------------------------------------------------------------------
| send email  Routes
|--------------------------------------------------------------------------
|
*/ 
// Route::get('/email', function ()
// {
//     Mail::to('liaqat@yahoo.com')->send(new OrderMail());
// });

/*
|--------------------------------------------------------------------------
| Products Routes
|--------------------------------------------------------------------------
|
*/
Route::get('products', [ProductController::class, 'index']);
Route::get('product/create', [ProductController::class, 'create'])->name('product.create');
Route::post('product/store', [ProductController::class, 'store'])->name('product.store');
Route::get('product/show/{id}', [ProductController::class, 'show'])->name('product.show');
Route::get('product/stock/{id}', [ProductController::class, 'getStockOfSingleProduct']);
Route::get('product/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
Route::post('product/update/{id}', [ProductController::class, 'update'])->name('product.update');
Route::get('product/delete/{id}', [ProductController::class, 'destroy'])->name('product.destroy');
Route::post('product/result', [ProductController::class, 'result']);

Route::get('suppliers', [SupplierController::class, 'index']);
Route::get('suppliers/create', [SupplierController::class, 'create']);
Route::post('suppliers/store', [SupplierController::class, 'store']);
Route::get('suppliers/edit/{id}', [SupplierController::class, 'edit']);
Route::post('suppliers/update/{id}', [SupplierController::class, 'update']);
Route::get('suppliers/delete/{id}', [SupplierController::class, 'destroy']);
Route::get('suppliers/search', [SupplierController::class, 'search']);
Route::post('suppliers/result', [SupplierController::class, 'result']);
Route::get('suppliers/show/{id}', [SupplierController::class, 'show']);
Route::get('suppliers/inventory/{id}', [SupplierController::class, 'inventoryshow']);


/*
|--------------------------------------------------------------------------
| Orders Routes
|--------------------------------------------------------------------------
*/
// Route::view('waste', 'orders.waste');

Route::get('orders', [OrderController::class, 'index']);
Route::get('orders/create', [OrderController::class, 'create']);
Route::get('orders/waste', [OrderController::class, 'wasteDetail']);
Route::get('orders/customer', [OrderController::class, 'customer']);
Route::post('orders/store', [OrderController::class, 'store']);
Route::post('orders/customerstore', [OrderController::class, 'customerstore']);
Route::post('orders/wasteorder', [OrderController::class, 'wasteOrder']);
Route::get('orders/edit/{id}', [OrderController::class, 'edit']);
Route::post('orders/update/{id}', [OrderController::class, 'update']);
Route::get('orders/delete/{id}', [OrderController::class, 'destroy']);
Route::get('orders/result', [OrderController::class, 'result']);
Route::get('orders/show/{id}', [OrderController::class, 'show']);
Route::get('orders/inventory/{id}', [OrderController::class, 'showinventory']);
Route::get('orders/inbound', [OrderController::class, 'show1']);
Route::get('orders/outbound', [OrderController::class, 'show2']);
Route::get('orders/export', [OrderController::class, 'export']);
Route::get('orders/csv', [OrderController::class, 'exportCSV']);
// Route::get('/email', [OrderController::class, 'SendMail']);
// Route::get('/mail', function(){
//     return new OrderMail();
// } );

/*
|--------------------------------------------------------------------------
| Customers Routes
|--------------------------------------------------------------------------
*/

Route::get('customers', [CustomerController::class, 'index']);
Route::get('customers/addcustomer', [CustomerController::class, 'create']);
Route::post('customers/store', [CustomerController::class, 'store']);
Route::post('customers/storeAjax', [CustomerController::class, 'storeAjax']);
Route::get('customers/edit/{id}', [CustomerController::class, 'edit']);
Route::post('customers/update/{id}', [CustomerController::class, 'update']);
Route::get('customers/delete/{id}', [CustomerController::class, 'destroy']);
Route::post('customers/result', [CustomerController::class, 'result']);
Route::get('customers/order/{id}', [CustomerController::class, 'show']);
Route::get('customers/inventory/{id}', [CustomerController::class, 'inventoryshow']);

/*
|--------------------------------------------------------------------------
| InventoryList Routes
|--------------------------------------------------------------------------
*/

Route::get('inventory',[InventoryController::class, 'index']);
Route::post('inventory/result', [InventoryController::class, 'result']);
Route::get('inventory/stock/{id}', [InventoryController::class, 'getData']);
Route::get('inventory/stock/', [InventoryController::class, 'getDataStock']);

