<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ url('/assets') }}/plugins/fontawesome-free/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ url('/assets') }}/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/assets') }}/dist/css/adminlte.min.css?v=3.2.0">
    <link rel="stylesheet" href="{{ url('/assets') }}/plugins/daterangepicker/daterangepicker.css">
    <!-- Styles -->
    <style>
        .container-fluid {
            padding-left: 15px;
            padding-right: 15px;
        }

        svg {
            height: 20px;
        }

    </style>

</head>

<body>
    @include('components.navbar')
    @include('components.sidebar-left')


    <div id="app">

        <main class="content-wrapper">

            @yield('content')


        </main>
        <x-footer />
    </div>

    {{-- java Script --}}
    <script src="{{ url('/assets') }}/plugins/jquery/jquery.min.js"></script>
    <script src="{{ url('/assets') }}/dist/js/bootstrap.min.js"></script>

    <script src="{{ url('/assets') }}/dist/js/adminlte.min.js?v=3.2.0"></script>
    <script src="{{ url('/assets') }}/plugins/daterangepicker/moment.min.js"></script>
    <script src="{{ url('/assets') }}/plugins/daterangepicker/daterangepicker.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select11').select2();
        });
    </script>
    <script>
        $(document).ready(function() {
            fetch_order_data();

            function fetch_order_data(query = '') {
                $.ajax({
                    url: "{{ url('live_search.action') }}",
                    method: 'get',
                    dataType: 'json',
                    data: {
                        query: query
                    },

                    success: function(data) {
                        $('tbody').html(data.table_data);
                        $('#total_records').text(data.total_records);
                    }
                })
            }
        });
    </script>
</body>

</html>
