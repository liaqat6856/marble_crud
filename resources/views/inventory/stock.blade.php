@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <!-- /.row -->
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="card">
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h2 class="float-left ">Stock</h2> <a href="{{ url('inventory') }}"
                                            class="float-right btn btn-primary btn-sx">Go Back</a>
                                    </div>

                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr class="bg-primary">
                                                    <th>Product Name</th>
                                                    <th>Type</th>
                                                    <th>length</th>
                                                    <th>Width</th>
                                                    <th>Qty</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($products as $product)
                                                    <tr>
                                                        <td>{{ @$product->product->name }}
                                                            ({{ @$product->product_name }})
                                                        </td>
                                                        <td>{{ $product->type }}</td>

                                                        <td>{{ @$product->total_length }}</td>
                                                        <td>{{ @$product->total_width }}</td>
                                                        <td>{{ @$product->total_qty }} ft</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!--/. container-fluid -->
    </section>
@endsection
