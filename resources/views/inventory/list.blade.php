@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <!-- /.row -->
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h1 class="card-title">Inventory</h1>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('inventory/result') }}" method="post">
                                @csrf
                                <div class="input-group input-group-sm mb-5">
                                    <input type="text" name="search" class="form-control">
                                    <span class="input-group-append">
                                        <button type="submit" class="btn btn-primary btn-flat"
                                            style="width: 300px;">Search</button>
                                    </span>
                                </div>
                            </form>
                            <table class="table table-striped">
                                <thead>
                                    <tr class="bg-primary">
                                        <th>Order ID</th>
                                        <th>Marble Name</th>
                                        <th>Type</th>
                                        <th>Length</th>
                                        <th>Width</th>
                                        <th>Qty</th>
                                        <th>Comments</th>
                                        <th>Order Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($lists as $list)
                                        <tr>
                                            <td>{{ $list->order_id }}</td>
                                            <td>{{ @$list->product->name }}</td>
                                            <td>{{ $list->type }}</td>
                                            <td>{{ str_replace('-', '', $list->length) }}</td>
                                            <td>{{ str_replace('-', '', $list->width) }}</td>
                                            <td>{{ str_replace('-', '', $list->qty) }}</td>
                                            <td>{{ $list->comments }}</td>
                                            @if ($list->order_type == 'inbound')
                                                <td>
                                                    <label for=""
                                                        class="btn btn-success btn-sm">{{ $list->order_type }}</label>
                                                </td>
                                            @else
                                                <td>
                                                    <label for=""
                                                        class="btn btn-danger btn-sm">{{ $list->order_type }}</label>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{-- <div class="container flex items-center justify-center" >{{$lists->links()}}</div> --}}
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!--/. container-fluid -->
    </section>
@endsection
