<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="home.html" class="brand-link">
        <img src="../assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">{{ config('app.Logo_title') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <!-- Sidebar Menu -->
            <nav class="mt-2" style="width: 100%;">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview">
                        <a href="{{ url('products') }}"
                            class="nav-link {{ request()->is('products') ? 'active' : '' }}">
                            <i class="fa fa-circle" style="padding-right: 10px;"> </i>
                            <p>Products</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{ url('inventory/stock') }}"
                            class="nav-link {{ request()->is('inventory/stock') ? 'active' : '' }}">
                            <i class="fa fa-circle" aria-hidden="true" style="padding-right: 10px;"></i>
                            <p>Stock</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{ url('inventory') }}"
                            class="nav-link {{ request()->is('inventory') ? 'active' : '' }}">
                            <i class="fa fa-circle" style="padding-right: 10px;"> </i>
                            <p> Inventory</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{ url('orders') }}"
                            class="nav-link {{ request()->is('orders') ? 'active' : '' }}">
                            <i class="fa fa-circle" style="padding-right: 10px;"> </i>
                            <p>Orders</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{ url('suppliers') }}"
                            class="nav-link {{ request()->is('suppliers') ? 'active' : '' }}">
                            <i class="fa fa-circle" style="padding-right: 10px;"> </i>
                            <p>Suppliers</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{ url('customers') }}"
                            class="nav-link {{ request()->is('customers') ? 'active' : '' }}">
                            <i class="fa fa-circle" style="padding-right: 10px;"> </i>
                            <p>Customers</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
</aside>
