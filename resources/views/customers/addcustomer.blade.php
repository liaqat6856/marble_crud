@extends('layouts.app')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="home.html">Home</a></li>
                        <li class="breadcrumb-item active">Customer Page</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <div class="col-md-12" style="margin-top: 30px;">
                <div class="card">
                    <div class="card-header ">
                        <h2 class="float-left">Add Customer</h2>
                        <a href="{{ url('customers') }}" type="button" class="btn bg-gradient-primary float-right">Back To
                            List</a>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <form action="{{ url('customers/store') }}" method="post" class="col-md-12">
                                @csrf
                                <!-- text input -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-md-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control "
                                                    placeholder="Enter ...">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-md-3 col-form-label">Address</label>
                                            <div class="col-md-9">
                                                <input type="text" name="address" class="form-control "
                                                    placeholder="Enter ...">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-3 col-form-label">Phone</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="phone" class="form-control "
                                                    placeholder="Enter ...">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn bg-gradient-secondary">Save</button>
                            </form>
                        </div>
                    </div>

                    <!-- /.card -->
                </div>



            </div><!-- /.container-fluid -->

        </div>
    @endsection
