@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-6">

                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Customer Orders Page</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <div class="col-md-12" style="margin-top: 30px;">
                <div class="card">
                    <div class="card-header card-primary">
                        <h1>Customers Orders <span class="float-right"><a href="/customers"
                                    class="btn btn-default btn-sm">Back</a></span></h1>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <table class="table table-striped">
                                <tr>
                                    <th>Order Id</th>
                                    <th>Date</th>
                                    <th>Customer ID</th>
                                    <th>Bill Id</th>
                                    <th>Comments</th>
                                    <th>Order Type</th>
                                    <th>inventory</th>
                                </tr>
                                @foreach ($shows as $show)
                                    <tr>
                                        <td>{{ $show->id }}</td>
                                        <td>{{ \Carbon\Carbon::parse($show->date)->format('m/d/Y') }}</td>
                                        <td>{{ $show->customer_id }}</td>
                                        <td>{{ $show->bill_id }}</td>
                                        <td>{{ $show->comments }}</td>
                                        <td>
                                            <a href="" class="btn btn-danger btn-sm">{{ $show->order_type }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ url('customers/inventory/' . $show->id) }}"
                                                class="btn btn-primary btn-sm">Inventory Detail</a>
                                        </td>

                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div> <!-- /.card -->
                </div>



            </div><!-- /.container-fluid -->

        </div>
    @endsection
