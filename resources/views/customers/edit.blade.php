@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 15px;">
    <div class="row ">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Customers <span class="float-right"><a href="/customers" class="btn btn-primary">Back</a></span></div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Edit Products</h3>
                    <form action="{{url('customers/update',$customers->id)}}" method="POST" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Customer Name</label>
                            <input type="text" name="name" value="{{$customers->name}}" class="form-control">
                            
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" name="address" value="{{$customers->address}}"  class="form-control">
                           
                        </div>
                        
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="text" name="phone" value="{{$customers->phone}}"   class="form-control">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Edit Customer</button>
                        </div>
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
