@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Customer Page</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <div class="col-md-12" style="margin-top: 30px;">
                <div class="card ">
                    <div class="card-header card-primary">
                        <h1 class="float-left">Customers</h1><a href="{{ url('orders') }}" type="button"
                        class="btn bg-gradient-primary float-right ml-3">Back</a>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-6">
                                <form action="{{url('customers?search')}}" method="get" class="ml-2">
                                    <div class="">
                                        <div class="form-group row">
                                            <input type="text" name="search" placeholder="Search By Name"
                                                class="form-control col-md-6">
                                            <input type="submit" value="Search" class="btn btn-default col-md-4 ml-2">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6 pl-10">
                                <a href="{{ url('customers/addcustomer') }}" type="button"
                                    class="btn bg-gradient-primary float-right ml-3">Add
                                    Customer</a>
                            </div>
                        </div>
                        <table class="table table-striped">
                            <tr class="bg-primary">
                                <th>Name</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Orders</th>
                                <th>Action</th>
                                <th>Records</th>
                            </tr>
                            @foreach ($customers as $customer)
                                <tr>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->address }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ count(@$customer->orders) }}</td>

                                    <td>
                                        <a href="{{ url('customers/update/' . $customer->id) }}" class=""><i
                                                class="fa fa-edit"></i></a>
                                        <a href="{{ url('customers/delete/' . $customer->id) }}" class="text-danger"><i
                                                class="fa fa-trash"></i></a>

                                    </td>
                                    <td><a href="{{ url('customers/order/' . $customer->id) }}"
                                            class="btn btn-primary btn-sm">History Record</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div> <!-- /.card -->
            </div>



        </div><!-- /.container-fluid -->

    </div>
@endsection
