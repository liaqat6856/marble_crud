@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="margin-top: 15px;">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="float-left">Orders</h4>
                        <span class="float-right"><a href="{{ url('orders/customer') }}" class="btn btn-primary"
                                style="margin-left: 10px;">Customer Order</a></span>
                        <span class="float-right"><a href="{{ url('/orders/create') }}" class="btn btn-primary ">Supplier
                                Order</a></span>
                                <span class="float-right mr-2"><a href="{{ url('orders/waste') }}" class="btn btn-primary"
                                    style="margin-left: 10px;">Waste Order</a></span>
                                    <span class="float-right mr-2"><a href="{{ url('orders/export') }}" class="btn btn-primary"
                                        style="margin-left: 10px;">Order Export</a></span>
                                        <span class="float-right mr-2"><a href="{{ url('orders/csv') }}" class="btn btn-primary"
                                            style="margin-left: 10px;">Order Csv</a></span>
                    </div>

                    <div class="card-body">
                        <div class="mb-2">
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="row">
                                        <a href="{{ url('orders?order_type=intboud') }}" class="btn btn-default btn-sm col-md-2 ml-2"
                                        style="height: 30px;">Inbound</a>
                                        <a href="{{ url('orders?order_type=outbound') }}" class="btn btn-default btn-sm col-md-2 ml-2"
                                            style="height: 30px;">Outbound</a>
                                            <a href="{{ url('orders?supplier_name') }}" class="btn btn-default btn-sm col-md-2 ml-2"
                                            style="height: 30px;">Supplier Nme</a>
                                            <a href="{{ url('orders?customer_name') }}" class="btn btn-default btn-sm col-md-2 ml-2"
                                            style="height: 30px;">Customer Nme</a>
                                        <form action="{{ url('orders?search') }}" method="GET" class="col-md-2 ml-2">
                                            <div class="form-group row">
                                                <input type="text" name="search" placeholder="Search" class="form-control col-md-8" 
                                                    style="height: 30px;" id="datepicker">
                                                <input type="submit" value="Search" class="btn btn-default ml-2 p-0 pl-2 pr-2"
                                                    style="height: 30px;">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <a href="{{ url('orders') }}" class="btn btn-primary btn-sm float-right"
                                    style="height: 30px;">Back</a>
                                </div>

                            </div>

                        </div>



                        <table class="table table-striped">

                            <tr class="bg-primary">
                                <th> Order ID</th>
                                <th>Date</th>
                                <th> Name</th>
                                <th>Bill ID</th>
                                <th>Comments</th>
                                <th>Action</th>
                                <th>inventories</th>
                                <th>Order Type</th>

                            </tr>
                            @foreach ($orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    {{-- <td>{{ \Carbon\Carbon::parse($order->date)->format('m/d/Y') }}</td> --}}
                                    <td>{{ get_formatted_date($order->date,'d-m-y')}}</td>
                                    <td>{{ @$order->suplier->name }} {{ @$order->customer->name }}</td>
                                    
                                    <td>{{ $order->bill_id }}</td>
                                    <td>{{ $order->comments }}</td>

                                    <td>
                                        <a href="{{ url('orders/edit/' . $order->id) }}" class=""><i
                                                class="fa fa-edit"></i></a>
                                        <a href="{{ url('orders/delete/' . $order->id) }}" class="text-danger "
                                            style="margin-left: 20px;"><i class="fa fa-trash"></i></a>
                                        <a href="{{ url('orders/show/' . $order->id) }}" class=""
                                            style="margin-left: 20px;"><i class="fa fa-eye"></i></a>
                                    </td>
                                    <td>
                                        <a href="{{ url('orders/inventory/' . @$order->id) }}" class=""
                                            style="margin-left: 20px;"><i class="fa fa-list"></i></a>
                                        <button
                                            class="btn btn-default btn-sm">{{ $order->inventories->count() }}</button>
                                    </td>
                                    <td>
                                        @if ($order->order_type == 'outbound')
                                            <button class="btn btn-danger btn-sm">{{ $order->order_type }}</button>
                                        @else
                                            <label class="btn btn-success btn-sm">{{ $order->order_type }}</label>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        </table>

                        {{-- <div>{{ $orders->links() }}</div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ url('/assets') }}/plugins/jquery/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $(function() {
                $('#datepicker').daterangepicker({
                    singleDatePicker: true
                });
            });
            $('#myForm').click(function(e) {

                e.preventDefault();
                let name = $('#name').val();
                let address = $('#address').val();
                let phone = $('#phone').val();
                $.ajax({
                    url: "{{ url('customers/storeAjax') }}",
                    method: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "name": name,
                        "address": address,
                        "phone": phone
                    },
                    success: function(result) {
                        console.log(result)


                    },
                    error: function() {

                    }

                });

                document.getElementById("ajexForm").reset();
            });


        });
    </script>

@endsection
