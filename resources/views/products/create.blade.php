@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="margin-top: 15px;">
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Products <span class="float-right"><a href="/products"
                                class="btn btn-primary">Back</a></span></div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Add Products</h3>
                                <form action="/product/store" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="">Product Name</label>
                                        <input type="text" name="name" class="form-control">
                                        @error('name')
                                            <span class="alert alert-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Type</label>
                                        <select name="type" class="form-control">

                                            <option value="Floor">Floor</option>
                                            <option value="Slab">Slab</option>
                                            <option value="Stairs">Stairs</option>
                                            <option value="Patti">Patti</option>
                                            <option value="Scarting">Scarting</option>
                                            <option value="Razor">Razor</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Product</label>
                                        <input type="file" name="image" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Comments</label>
                                        <textarea id="" cols="30" rows="3" class="form-control" name="comments"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Add Product</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
