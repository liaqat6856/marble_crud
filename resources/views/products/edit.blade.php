@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="margin-top: 15px;">
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Prouducts <span class="float-right"><a href="/products"
                                class="btn btn-primary">Back</a></span></div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Edit Products</h3>

                                <form action="{{ url('/product/update/' . $product->id) }}" method="POST"
                                    enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="">Product Name</label>
                                        <input type="text" name="name" value="{{ $product->name }}"
                                            class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Type</label>
                                        <input type="text" name="type" value="{{ $product->type }}"
                                            class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Product File</label>
                                        <input type="file" name="image" value="{{ $product->image }}"
                                            class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Comments</label>
                                        <textarea id="" cols="30" rows="3" class="form-control" name="comments">  {{ $product->comments }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Update Product</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
