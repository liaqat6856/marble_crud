@extends('layouts.app')


@section('content')
    <section class="content">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-2 ml-3 text-dark"> Products Stock</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right m-2">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Products</li>
                </ol>
            </div><!-- /.col -->
        </div>
        <div class="container-fluid">
            <!-- <div class="row"> -->
            <!-- <div class="col-12"> -->
            <div class=" card">
                <div class="card-header">
                    @foreach ($inventory as $list)
                        <h3 class="float-left">
                            {{ @$product->name }}
                        </h3>
                        <a href="/products" class="btn btn-primary float-right" style="color: #000;">Back</a>
                </div>
                <div class="card-body">


                    <table class="table table-striped">
                        <tr class="bg-primary">
                            <th>name</th>
                            <th>width</th>
                            <th>length</th>
                            <th>qty</th>
                        </tr>


                        <tr>
                            <td>{{ @$product->name }}</td>
                            <td>{{ @$list->total_width }}</td>
                            <td>{{ @$list->total_length }}</td>
                            <td>{{ @$list->total_qty }}</td>
                        </tr>
                        @endforeach


                    </table>
                    <hr>
                    <div class="card-body mt-12">
                        <h1>Product Detail</h1>
                        <table class="table table-striped ">
                            <tr class="bg-primary">
                                <th>Order ID</th>
                                <th>Type</th>
                                <th>Length</th>
                                <th>Width</th>
                                <th>Qty</th>
                                <th>Comments</th>
                                <th>Order Type</th>
                            </tr>
                            @foreach ($inventories as $inventory)
                                <tr>
                                    <td>{{ $inventory->order_id }}</td>
                                    <td>{{ $inventory->type }}</td>
                                    <td>{{ str_replace('-', '', $inventory->length) }}</td>
                                    <td>{{ str_replace('-', '', $inventory->width) }}</td>
                                    <td>{{ str_replace('-', '', $inventory->qty) }}</td>
                                    <td>{{ $inventory->comments }}</td>
                                    @if ($inventory->order_type == 'inbound')
                                        <td>
                                            <label for=""
                                                class="btn btn-success btn-sm">{{ $inventory->order_type }}</label>
                                        </td>
                                    @else
                                        <td>
                                            <label for=""
                                                class="btn btn-danger btn-sm">{{ $inventory->order_type }}</label>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach




                        </table>
                    </div>


                </div>

                <!-- </div> -->
                <!--  </div> -->
            </div>

        </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
