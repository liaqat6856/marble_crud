@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="row mb-2 m-1">
            <div class="col-sm-6">
                <h1 class="m-2 text-dark">Our Products</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right m-2">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Products</li>
                </ol>
            </div><!-- /.col -->
        </div>
        <div class="container-fluid">
            <!-- <div class="row"> -->
            <!-- <div class="col-12"> -->
            <div class=" card">
                <div class="card-header">
                    <div class="card-title">
                        Our Products
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <div class="mb-2 mt-2">
                            <a class="btn btn-primary" href="{{ url('product/create') }}"> Add Products </a>
                            <div class="float-right">
                                <form action="products/result" method="post">
                                    @csrf
                                    <div class="float-right ">
                                        <div class="form-group row">
                                            <input type="text" name="search" placeholder="Search By Name"
                                                class="form-control col-md-6">
                                            <input type="submit" value="Search" class="btn btn-default col-md-4">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div>
                        <table class="table table-striped">
                            <tr class="bg-primary">
                                <th>Name</th>
                                <th>Types</th>
                                <th>comments</th>
                                <th>File</th>
                                <th>Action</th>
                            </tr>
                            @foreach ($products as $product)
                                <tr>
                                    <td><a href="{{ url('product/show/' . $product->id) }}">{{ $product->name }}</a>
                                    </td>
                                    <td>{{ $product->type }}</td>
                                    <td>{{ $product->comments }}</td>
                                    <td>
                                        <image height="40px" src="{{ asset('/storage/products/' . $product->image) }}" />
                                    </td>
                                    <td>
                                        <a href="{{ url('product/edit/' . $product->id) }}" class=""><i
                                                class="fa fa-edit"></i></a>
                                        <a href="{{ url('product/delete/' . $product->id) }}" class="text-danger"><i
                                                class="fa fa-trash"></i></a>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    </div>

                    <!-- </div> -->
                    <!--  </div> -->
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
