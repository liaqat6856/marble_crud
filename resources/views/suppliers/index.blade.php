@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-2 text-dark">Suppliers</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right m-2">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Supplier</li>
                </ol>
            </div><!-- /.col -->
        </div>
        <div class="container-fluid">
            <!-- <div class="row"> -->
            <!-- <div class="col-12"> -->
            <div class="card ">
                <div class="card-header">
                    <div >
                        <h3 class="float-left">Sppliers</h3> <a href="{{ url('orders') }}" class="btn btn-primary btn-sm float-right">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <form action="{{url('suppliers?search')}}" method="get"  class="float-left">
                            <div >
                                <div class="form-group row">
                                    <input type="text" name="search" placeholder="Search By Name"
                                        class="form-control col-md-6">
                                    <input type="submit" value="Search" class="btn btn-success col-md-4 ml-2">
                                </div>
                            </div>
                        </form>
                        <div class="mb-2 float-right">
                            <a class="btn btn-success" href="/suppliers/create"> Add Supplier </a>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <tr class="bg-primary">
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Orders</th>
                            <th>Action</th>
                            <th>History</th>
                        </tr>
                        @foreach ($suppliers as $supplier)
                            <tr>
                                <td>{{ $supplier->name }}</td>
                                <td>{{ $supplier->address }}</td>
                                <td>{{ $supplier->phone }}</td>
                                <td>{{ @$supplier->order->count() }}</td>
                                <td>
                                    <a href="{{ url('suppliers/edit/' . $supplier->id) }}" class="text-primary"><i
                                            class="fa fa-edit"></i></a>
                                    <a href="{{ url('suppliers/delete/' . $supplier->id) }}" class="text-danger"><i
                                            class="fa fa-trash"></i></a>
                                </td>
                                <td><a href="{{ url('suppliers/show/' . $supplier->id) }}"
                                        class="btn btn-primary btn-sm">History Record</a></td>
                            </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
