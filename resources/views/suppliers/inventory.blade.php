@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-6">

                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Inventory Page</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <div class="col-md-12" style="margin-top: 30px;">
                <div class="card card-primary">
                    <div class="card-header card-primary">
                        <h1>Invetory Detail <span class="float-right"><a href="/suppliers"
                                    class="btn btn-success btn-sm">Back</a></span></h1>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <table class="table table-striped">
                                <tr class="tr-primary">
                                    <th>Order Id</th>
                                    <th>Product Name</th>
                                    <th>Type</th>
                                    <th>Length</th>
                                    <th>Width</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>Comments</th>
                                </tr>
                                @foreach ($inventoryds as $inventoryd)
                                    <tr>
                                        <td>{{ $inventoryd->order_id }}</td>
                                        <td>{{ @$inventoryd->product->name }}</td>
                                        <td>{{ $inventoryd->type }}</td>
                                        <td>{{ $inventoryd->length }}</td>
                                        <td>{{ $inventoryd->width }}</td>
                                        <td>{{ $inventoryd->qty }}</td>
                                        <td>{{ $inventoryd->price }}</td>
                                        <td>{{ $inventoryd->comments }}</td>

                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div> <!-- /.card -->
                </div>



            </div><!-- /.container-fluid -->

        </div>
    @endsection
