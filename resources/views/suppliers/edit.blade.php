@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 15px;">
    <div class="row ">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Suppliers <span class="float-right"><a href="/suppliers" class="btn btn-primary">Back</a></span></div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Edit Supplier</h3>
                    <form action="{{url('suppliers/update',$suppliers->id)}}" method="POST" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Supplier Name</label>
                            <input type="text" name="name" value="{{$suppliers->name}}" class="form-control">
                            
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" name="address" value="{{$suppliers->address}}"  class="form-control">
                           
                        </div>
                        
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="text" name="phone" value="{{$suppliers->phone}}"   class="form-control">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Edit Supplier</button>
                        </div>
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
