@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 15px;">
    <div class="row ">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">Supplliers <span class="float-right"><a href="/suppliers" class="btn btn-success">Back</a></span></div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Add Suppllier</h3>
                                <form action="{{url('suppliers/store')}}" method="POST"  enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="">Suppllier Name</label>
                                        <input type="text" name="name" class="form-control">
                                        @error('name')
                                                <span class="alert alert-danger" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Address</label>
                                        <input type="text" name="address" class="form-control">
                                        @error('type')
                                                <span class="alert alert-danger" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">phone</label>
                                        <input type="text" name="phone" class="form-control">
                                        @error('type')
                                                <span class="alert alert-danger" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Add Suppllier</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
