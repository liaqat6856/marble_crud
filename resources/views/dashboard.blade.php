@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 15px;">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }} <span class="float-right"><a href="/product/create" class="btn btn-primary">Add Products</a></span></div>

                <div class="card-body">
                    <h1>This is Dashboard Page</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
