@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Supplier Orders</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Orders</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <form class="form-horizontal" action="{{ url('orders/store') }}" method="POST" id="">
                <div class="card">
                    <div class="card-body">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Date</label>
                                    <div class="col-sm-9">
                                        <input type="date" name="date" class="form-control" id="inputEmail3"
                                            placeholder="Date Receive">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Suplier Name</label>
                                    <div class="col-sm-5">
                                        <select name="supliern" class="form-control js-example-basic-single"
                                            style="max-width: 300px;">
                                            @foreach ($suppliers as $supplier)
                                                <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" id="popup1" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus" style="margin-top: 10px;"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Bill ID.</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="bill" class="form-control" id="inputEmail3"
                                            placeholder="Bill ID">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Vehicle No.</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="vehicle" class="form-control" id="inputEmail3"
                                            placeholder="Vehicle No.">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Comments</label>
                                    <div class="col-sm-9">
                                        <textarea name="order_comments" cols="50" rows="1" id="inputEmail3" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="hidden" name="order_type" class="form-control" id="inputEmail3"
                                            placeholder="Vehicle No.">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-primary">
                        <h2>Product Receive Details</h2>
                    </div>
                    <!-- .card-header -->
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input id="addrow" value="Add New" type="button" class="btn btn-success">
                                    <table id="dynamicAddRemove">
                                        <thead>
                                            <tr role="row">
                                                <th style="width: 150px;">Product Name</th>
                                                <th>Type</th>
                                                <th>Length</th>
                                                <th>Width</th>
                                                <th>Qty</th>
                                                <th>Price</th>
                                                <th>Amount Rs.</th>
                                                <th>Comments</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <select name="product_id[]" class="form-control"
                                                        style="max-width: 300px;">
                                                        @foreach ($products as $product)
                                                            <option value="{{ $product->id }}"> {{ $product->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="type[]" class="form-control" style="min-width: 105px;">

                                                        <option value="Floor">Floor</option>
                                                        <option value="Slab">Slab</option>
                                                        <option value="Stairs">Stairs</option>
                                                        <option value="Patti">Patti</option>
                                                        <option value="Scarting">Scarting</option>
                                                        <option value="Razor">Razor</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="length" class="form-control" type="text" name="length[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="width[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="qty[]">
                                                </td>

                                                <td>
                                                    <input class="form-control" type="text" name="price[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="totalrs[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="comments[]">
                                                </td>
                                                <td style="text-align: center;">
                                                    <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select name="product_id[]" class="form-control"
                                                        style="max-width: 300px;">
                                                        @foreach ($products as $product)
                                                            <option value="{{ $product->id }}"> {{ $product->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="type[]" class="form-control" style="min-width: 105px;">

                                                        <option value="Floor">Floor</option>
                                                        <option value="Slab">Slab</option>
                                                        <option value="Stairs">Stairs</option>
                                                        <option value="Patti">Patti</option>
                                                        <option value="Scarting">Scarting</option>
                                                        <option value="Razor">Razor</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="length" class="form-control" type="text" name="length[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="width[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="qty[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="price[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="totalrs[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="comments[]">
                                                </td>
                                                <td style="text-align: center;">
                                                    <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div style="margin-top: 20px;">
                                        <button type="submit" class="btn  btn-success">Order Save</button>
                                        <a href="/orders/create" class="btn  btn-info">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </form>
    </div>
    </div>
    {{-- <script src="{{ url('/assets') }}/plugins/jquery/jquery.min.js"></script> --}}
    {{-- <script>

   $(document).ready(function(){
    $("#addrow").click(function(){
        $("#dynamicAddRemove").append("<tr><td> <select type='text' class='form-control' name='product_id[]'>@foreach ($products as $product)<option value='{{$product->id}}'>{{$product->name}}</option>@endforeach</select> </td><td><select type='text' class='form-control' name=type[]'><option value='1'>Floor</option><option value='2'>Slab</option><option value='3'>Stairs</option><option value='4'>Patti</option><option value='5'>Scarting</option><option value='6'>Razor</option><option value='7'>Other</option></select></td><td><input  class='form-control' type='text' name='length[]'></td><td><input  class='form-control' type='text' name='width[]'></td><td><input  class='form-control' type='text' name='qty[]'></td><td><input  class='form-control' type='text' name='price[]'></td><td><input  class='form-control' type='text' name='totalrs[]'></td><td><input  class='form-control' type='text' name='comments[]'></td><td><button  class='btn btn-default btn-info remove-tr'><i class='fa fa-trash'></i></button></td></tr>");
    });

    $("#dynamicAddRemove").on('click', '.remove-tr', function(){
        $(this).parent().parent().remove();
    });

   $('#myForm').click(function(e){
            e.preventDefault();
            let name = $('#name').val();
            let address = $('#address').val();
            let phone = $('#phone').val();
            $.ajax({
                url:"{{ url('suppliers/store') }}",
                method: "post",
                data: {
                  "_token": "{{ csrf_token() }}",
                  "name": name,
                  "address": address,
                  "phone": phone
                  },
                success: function(){
                    alert('Added Supplier');
                    window.location.href = "{{ url('orders/create')}}";
                }
            });
        });

      });
</script> --}}
@endsection
