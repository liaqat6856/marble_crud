@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <!-- /.row -->
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header border-0">
                            <div>Inventory Detail</h1>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-header">Inventory <a href="{{ url('orders') }}"
                                                class="float-right btn btn-default btn-sx">Go Back</a></div>

                                        <div class="card-body">
                                            <ul class="list-group">
                                                @foreach ($inventories as $inventory)
                                                    <li class="list-group-item">Type: {{ $inventory->type }}</li>
                                                    <li class="list-group-item">Length: {{ $inventory->length }}</li>
                                                    <li class="list-group-item">Width: {{ $inventory->width }}</li>
                                                    <li class="list-group-item">Qty: {{ $inventory->qty }}</li>
                                                    <li class="list-group-item">Price: {{ $inventory->price }}</li>
                                                    <li class="list-group-item">Total Rs:: {{ $inventory->totelRs }}</li>
                                                @endforeach
                                            </ul>
                                            <div class="Jumbotron">
                                                {{ $inventory->comments }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!--/. container-fluid -->
    </section>
@endsection
