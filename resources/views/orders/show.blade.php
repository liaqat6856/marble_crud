@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <!-- /.row -->
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h1 class="">Orders Detail</h1>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-header">{{ $orders->bill_id }} <a href="{{ url('orders') }}"
                                                class="float-right btn btn-default btn-sx">Go Back</a></div>

                                        <div class="card-body">
                                            <ul class="list-group">
                                                <li class="list-group-item">Date:
                                                    {{ \Carbon\Carbon::parse($orders->date)->format('m/d/Y') }}</li>
                                                <li class="list-group-item">Supplier: {{ $orders->suplier_id }}</li>
                                                <li class="list-group-item">Vehcle No: {{ $orders->vehicle_no }}</li>
                                            </ul>
                                            <div class="Jumbotron">
                                                {{ $orders->comments }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!--/. container-fluid -->
    </section>
@endsection
