@extends('layouts.app')


@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Customer Orders</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Orders</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <form class="form-horizontal" action="{{ url('orders/customerstore') }}" method="POST">
                @csrf
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Date</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="date" class="form-control" id="inputEmail3">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Customer Name</label>
                                    <div class="col-sm-5">
                                        <select name="customer_id" class="form-control  select11" style="max-width: 300px;">
                                            @foreach ($customers as $customer)
                                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" id="popup1" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus" style="margin-top: 10px;"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- The Modal -->

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Bill ID.</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="bill" class="form-control" id="inputEmail3"
                                            placeholder="Bill ID">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Comments</label>
                                    <div class="col-sm-9">
                                        <textarea name="order_comments" cols="50" rows="1" id="inputEmail3" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-primary" >
                        <h2>Product Receive Details</h2>
                    </div>
                    <!-- .card-header -->
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input id="addrow" value="Add New" type="button" class="btn btn-success">
                                    <div class="float-right">
                                        @error('type')
                                            <span class="alert alert-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <table id="dynamicAddRemove">
                                        <thead>
                                            <tr role="row">
                                                <th style="width: 150px;">Product Name</th>
                                                <th>Type</th>
                                                <th>Length</th>
                                                <th>Width</th>
                                                <th>Qty</th>
                                                <th>Price</th>
                                                <th>Amount Rs.</th>
                                                <th>Comments</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <select name="product_id[]" class="form-control select1"
                                                        style="max-width: 300px;">
                                                        @foreach ($products as $product)
                                                            <option value="{{ $product->id }}"> {{ $product->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="type[]" class="form-control select2"
                                                        style="min-width: 105px;">

                                                        <option value="Floor">Floor</option>
                                                        <option value="Slab">Slab</option>
                                                        <option value="Stairs">Stairs</option>
                                                        <option value="Patti">Patti</option>
                                                        <option value="Scarting">Scarting</option>
                                                        <option value="Razor">Razor</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </td>
                                                <td class="test">
                                                    <input id="length" class="form-control length" type="text"
                                                        name="length[]">
                                                </td>
                                                <td class="test">
                                                    <input class="form-control width" type="text" name="width[]">
                                                </td>
                                                <td class="show_erorr">
                                                    <input
                                                        class="form-control {{ $errors->first('qty[]') ? ' form-error' : '' }}"
                                                        type="text" id="grand" name="qty[]">
                                                    @if (session()->has('error'))
                                                        <p id="grand" class="text-red">
                                                            {{ session()->get('error') }}</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="price[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="totalrs[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="comments[]">
                                                </td>
                                                <td style="text-align: center;">
                                                    <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select name="product_id[]" class="form-control"
                                                        style="max-width: 300px;">
                                                        @foreach ($products as $product)
                                                            <option value="{{ $product->id }}"> {{ $product->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="type[]" class="form-control" style="min-width: 105px;">

                                                        <option value="Floor">Floor</option>
                                                        <option value="Slab">Slab</option>
                                                        <option value="Stairs">Stairs</option>
                                                        <option value="Patti">Patti</option>
                                                        <option value="Scarting">Scarting</option>
                                                        <option value="Razor">Razor</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </td>
                                                <td class="test">
                                                    <input id="length" class="form-control length" type="text"
                                                        name="length[]">
                                                </td>
                                                <td class="test">
                                                    <input class="form-control width" type="text" name="width[]">
                                                </td>
                                                <td class="show_erorr">
                                                    <input
                                                        class="form-control {{ $errors->first('qty[]') ? ' form-error' : '' }}"
                                                        type="text" id="grand" name="qty[]">
                                                    @if (session()->has('error'))
                                                        <p id="grand" class="text-red">
                                                            {{ session()->get('error') }}</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="price[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="totalrs[]">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="comments[]">
                                                </td>
                                                <td style="text-align: center;">
                                                    <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div style="margin-top: 20px;">
                                        <button type="submit" class="btn  btn-success">Order Save</button>
                                        <a href="/orders/create" class="btn  btn-info">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </form>
    </div>
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add Customers</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="{{ url('suppliers/store') }}" method="POST" id="ajexForm">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Customer Name</label>
                            <input type="text" id="name" name="name" class="form-control">
                            @error('name')
                                <span class="alert alert-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" id="address" name="address" class="form-control">
                            @error('type')
                                <span class="alert alert-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">phone</label>
                            <input type="text" id="phone" name="phone" class="form-control">
                            @error('type')
                                <span class="alert alert-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button id="myForm" type="submit" class="btn btn-primary btn-block">Add Customer</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <style>
        .show_erorr {
            position: relative;
        }

        .show_erorr p {
            position: absolute;
            position: absolute;
            z-index: 9999;
            top: 2px;
            font-size: 16px;
            background: chartreuse;
            height: 36px;
            padding: 6px;
            width: 99%;
            font-weight: 600;
        }

    </style>
    </div>
    <script src="{{ url('/assets') }}/plugins/jquery/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $(function() {
                $('input[name="date"]').daterangepicker({
                    singleDatePicker: true
                });
            });
            $('#myForm').click(function(e) {
                $("#myForm").attr("disabled", true);

                e.preventDefault();
                let name = $('#name').val();
                let address = $('#address').val();
                let phone = $('#phone').val();
                $.ajax({
                    url: "{{ url('customers/storeAjax') }}",
                    method: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "name": name,
                        "address": address,
                        "phone": phone
                    },
                    success: function(result) {
                        console.log(result)
                        var data = {
                            id: result.id,
                            text: result.name,
                        };
                        var newOption = new Option(data.text, data.id, false, true);
                        $('.select11').append(newOption).trigger('change');

                    },
                    error: function() {
                        $("#myForm").attr("disabled", false);
                    }

                });

                document.getElementById("ajexForm").reset();
            });


        });
    </script>
@endsection
