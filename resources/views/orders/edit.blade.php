@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 15px;">
    <div class="row ">
        <div class="col-md-1">
        
        @section('content')
        <div class="content-header">
          <div class="container-fluid">
             <div class="row mb-2">
                <div class="col-sm-6">
                   <h1 class="m-0 text-dark">Receive Orders</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                   <ol class="breadcrumb float-sm-right">
                      <li class="breadcrumb-item"><a href="/">Home</a></li>
                      <li class="breadcrumb-item active">Orders</li>
                   </ol>
                </div>
                <!-- /.col -->
             </div>
             <form class="form-horizontal" action="{{url('orders/update',$orders->id)}}" method="POST">
               <div class="card">
                <div class="card-body">
                  
                      @csrf
                      <div class="row">
                         <div class="col-md-6">
                            <div class="form-group row">
                               <label for="inputEmail3" class="col-sm-3 col-form-label">Date</label>
                               <div class="col-sm-9">
                                  <input type="date" name="date" value="{{$orders->date}}" class="form-control" id="inputEmail3" placeholder="Date Receive">
                               </div>
                            </div>
                         </div>
                         <div class="col-md-6">
                          <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-3 col-form-label">Suplier ID</label>
                             <div class="col-sm-9">
                                <select name="supliern" value="{{$orders->supplier_id}}" class="form-control" style="max-width: 300px;">
                                   @foreach ($suppliers as $supplier)
                                      <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                   @endforeach
                                 </select>
                             </div>
                          </div>
                       </div>
                      </div>
                      <div class="row">
                         <div class="col-md-6">
                            <div class="form-group row">
                               <label for="inputEmail3" class="col-sm-3 col-form-label">Bill ID.</label>
                               <div class="col-sm-9">
                                  <input type="text" value="{{$orders->bill_id}}" name="bill" class="form-control" id="inputEmail3" placeholder="Bill ID">
                               </div>
                            </div>
                         </div>
                         <div class="col-md-6">
                            <div class="form-group row">
                               <label for="inputEmail3" class="col-sm-3 col-form-label">Vehicle No.</label>
                               <div class="col-sm-9">
                                  <input type="text" value="{{$orders->vehcile_no}}" name="vehicle" class="form-control" id="inputEmail3" placeholder="Vehicle No.">
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="row">
                         <div class="col-md-6">
                            <div class="form-group row">
                               <label for="inputEmail3" class="col-sm-3 col-form-label">Comments</label>
                               <div class="col-sm-9">
                                  <textarea name="comments" cols="50" rows="1" id="inputEmail3" class="form-control">{{$orders->comments}}</textarea>
                               </div>
                            </div>
                         </div>
                      </div>
                    </div>
                 </div>
               </div>
               <div class="row">
                <div class="col-12">
                   <div class="card">
                      <div class="card-header" style="background: #C6B100;text-align: center;">
                        <h2>Product Receive Details</h2>
                      </div>
        
                          <!-- .card-header -->
                      <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                           <div class="row">
                              <div class="col-sm-12">
                                <button class="btn btn-success">Add New</button>
                                  <table>
                                   <thead>
                                      <tr role="row">
                                         <th style="width: 150px;">Product Name</th>
                                         <th>Type</th>
                                         <th>Length</th>
                                         <th>Width</th>
                                         <th>Qty</th>
                                         <th>Price</th>
                                         <th>Amount Rs.</th>
                                         <th>Comments</th>
                                         <th>Delete</th>
                                      </tr>
                                   </thead>
                                   <tbody>
                                      <tr>
                                         <td>
                                            <select name="product_id" value="{{$inventory->product_name}}" class="form-control" style="max-width: 300px;">
                                              @foreach ($products as $product)
                                                 <option value="{{$product->id}}">{{$product->id}} {{$product->name}}</option>
                                              @endforeach
                                            </select> 
                                         </td>
                                         <td>
                                            <select name="type" value="{{$inventory->type}}" class="form-control" style="min-width: 105px;">
                                           
                                               <option value="1">Floor</option>
                                               <option value="2">Slab</option>
                                               <option value="3">Stairs</option>
                                               <option value="4">Patti</option>
                                               <option value="5">Scarting</option>
                                               <option value="6">Razor</option>
                                               <option value="7">Other</option>
                                            </select> 
                                         </td>
                                         <td>
                                            <input class="form-control" type="text" value="{{$inventory->length}}" name="length">
                                         </td>
                                         <td>
                                            <input class="form-control" type="text" value="{{$inventory->width}}" name="width">
                                         </td>
                                         <td>
                                            <input class="form-control" type="text" value="{{$inventory->qty}}" name="qty">
                                         </td>
                                         <td>
                                            <input class="form-control" type="text" value="{{$inventory->price}}" name="price">
                                         </td>
                                         <td>
                                            <input class="form-control" type="text" value="{{$inventory->totalrs}}"  name="totalrs">
                                         </td>
                                         <td>
                                            <input class="form-control" type="text" value="{{$inventory->comments}}"  name="comments">
                                         </td>
                                         <td style="text-align: center;">
                                            <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                         </td>
                                      </tr>
                                   </tbody>
                                  </table>
                                   <div style="margin-top: 20px;">
                                      <button type="submit" class="btn  btn-success">Order Save</button>
                                   
                                   
                                      <button type="button" class="btn  btn-info">Cancel</button>
                                   </div>
                              </div>
                           </div>
                         </div>
                       </div>  
                      </div>
                   </div>
                </div>
            
             </form>
           </div>
         </div>
        @endsection2">
            <div class="card">
                <div class="card-header">Orders <span class="float-right"><a href="/orders" class="btn btn-primary">Back</a></span></div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Edit Products</h3>
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
