
<h1>Order Notification!</h1>



<table border=1px>
    <thead>
        <tr>
            <td>Name</td>
            <td>Width</td>
            <td>Length</td>
            <td>Qty</td>
            <td>Price</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$order->customer_id}}{{$order->suplier_id}}</td>
            <td>{{$order->width}}</td>
            <td>{{$order->length}}</td>
            <td>{{$order->qty}}</td>
            <td>{{$order->price}}</td>
        </tr>
    </tbody>
</table>


Thanks,<br>
{{ config('app.name') }}

